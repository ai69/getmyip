package getmyip

import (
	"context"
	"errors"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

var (
	publicClient = &http.Client{
		Transport: &insecureTransport,
	}
	publicAPIs = []string{
		// IPv4 only
		"http://myexternalip.com/raw",
		"http://ifconfig.me/ip",
		"http://api.ipify.org",
		"http://ipaddress.sh/",
		// Force IPv4
		"http://ipv4.icanhazip.com/",
		"http://v4.ident.me/",
		"http://ip4.seeip.org/",
	}
)

// GetPublicIP retrieves public IPv4 address of current machine by calling various online services.
func GetPublicIP() (net.IP, error) {
	return parallelProcess(publicAPIs, queryPublicIPFunc(publicClient), TimeOutPublicIP)
}

// GetPublicIPWithClient retrieves public IPv4 address of current machine by calling various online services with given client and timeout.
func GetPublicIPWithClient(cli *http.Client, timeout time.Duration) (net.IP, error) {
	if cli == nil {
		// use default client as fallback
		cli = publicClient
	}
	return parallelProcess(publicAPIs, queryPublicIPFunc(cli), timeout)
}

func queryPublicIPFunc(client *http.Client) func(context.Context, string) (net.IP, error) {
	return func(ctx context.Context, api string) (net.IP, error) {
		req, err := http.NewRequest(http.MethodGet, api, nil)
		if err != nil {
			return nil, err
		}

		resp, err := client.Do(req.WithContext(ctx))
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		raw, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		ip := net.ParseIP(strings.TrimSpace(string(raw)))
		if ip == nil {
			return nil, errors.New("invalid IP address")
		}
		return ip, nil
	}
}
