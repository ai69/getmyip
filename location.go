package getmyip

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"sort"
	"strings"
	"sync"

	"github.com/antchfx/jsonquery"
	"golang.org/x/sync/errgroup"
)

// Geolocation describes the deduced geographic position and ISP info of an IP address.
type Geolocation struct {
	CountryCode string `json:"country_code,omitempty"`
	CountryFlag string `json:"country_flag,omitempty"`
	Country     string `json:"country,omitempty"`
	Region      string `json:"region,omitempty"`
	City        string `json:"city,omitempty"`
	Latitude    string `json:"latitude,omitempty"`
	Longitude   string `json:"longitude,omitempty"`
	Carrier     string `json:"carrier,omitempty"`
	ISP         string `json:"isp,omitempty"`
	ASN         string `json:"asn,omitempty"`
}

// GeolocationServiceProvider represents the name of service provider of Geolocation API.
type GeolocationServiceProvider string

// Name of service provider of Geolocation API
const (
	ServiceProviderIpregistry GeolocationServiceProvider = "ipregistry"
	ServiceProviderIpify      GeolocationServiceProvider = "ipify"
	ServiceProviderIpapiCo    GeolocationServiceProvider = "ipapico"
	ServiceProviderIpApiCom   GeolocationServiceProvider = "ipapicom"
	ServiceProviderIpdata     GeolocationServiceProvider = "ipdata"
)

// GeolocationResult represents a collection of geolocation result from various service providers.
type GeolocationResult map[GeolocationServiceProvider]*Geolocation

// GeolocationAccessTokens represents a mapping between geolocation service provider and access token to its service.
type GeolocationAccessTokens map[GeolocationServiceProvider]string

type geolocSvcProviderConfig struct {
	urlTemplate string
	fieldPath   *Geolocation
}

const licensePlaceholder = "[LICENSE]"

var (
	geolocSvcProviders = map[GeolocationServiceProvider]*geolocSvcProviderConfig{
		ServiceProviderIpregistry: {
			urlTemplate: `http://api.ipregistry.co/%s?key=[LICENSE]`,
			fieldPath: &Geolocation{
				CountryCode: "/location/country/code",
				CountryFlag: "/location/country/flag/emoji",
				Country:     "/location/country/name",
				Region:      "/location/region/name",
				City:        "/location/city",
				Latitude:    "/location/latitude",
				Longitude:   "/location/longitude",
				Carrier:     "/carrier/name",
				ISP:         "/connection/organization",
				ASN:         "/connection/asn",
			},
		},
		ServiceProviderIpify: {
			urlTemplate: `http://geo.ipify.org/api/v1?apiKey=[LICENSE]&ipAddress=%s`,
			fieldPath: &Geolocation{
				CountryCode: "/location/country",
				Region:      "/location/region",
				City:        "/location/city",
				Latitude:    "/location/lat",
				Longitude:   "/location/lng",
				Carrier:     "/as/name",
				ISP:         "/isp",
				ASN:         "/as/asn",
			},
		},
		ServiceProviderIpapiCo: {
			urlTemplate: `http://ipapi.co/%s/json/`,
			fieldPath: &Geolocation{
				CountryCode: "/country_code",
				Country:     "/country_name",
				Region:      "/region",
				City:        "/city",
				Latitude:    "/latitude",
				Longitude:   "/longitude",
				ISP:         "/org",
				ASN:         "/asn",
			},
		},
		ServiceProviderIpApiCom: {
			urlTemplate: `http://ip-api.com/json/%s`,
			fieldPath: &Geolocation{
				CountryCode: "/countryCode",
				Country:     "/country",
				Region:      "/regionName",
				City:        "/city",
				Latitude:    "/lat",
				Longitude:   "/lon",
				ISP:         "/isp",
				ASN:         "/as",
			},
		},
		ServiceProviderIpdata: {
			urlTemplate: `https://api.ipdata.co/%s?api-key=[LICENSE]`,
			fieldPath: &Geolocation{
				CountryCode: "/country_code",
				CountryFlag: "/emoji_flag",
				Country:     "/country_name",
				Region:      "/region",
				City:        "/city",
				Latitude:    "/latitude",
				Longitude:   "/longitude",
				Carrier:     "/carrier/name",
				ISP:         "/asn/name",
				ASN:         "/asn/asn",
			},
		},
	}
)

// GetGeolocation returns deduced geographic position of a given IP address from licensed service providers.
func GetGeolocation(ip net.IP, tokens ...GeolocationAccessTokens) (GeolocationResult, error) {
	// check if the ip is nil
	if ip == nil {
		return nil, errors.New("ip address is nil")
	}

	// merge all token configs
	allToken := make(GeolocationAccessTokens)
	for _, m := range append([]GeolocationAccessTokens{DefaultGeolocationToken}, tokens...) {
		for k, v := range m {
			allToken[k] = v
		}
	}
	if len(allToken) == 0 {
		return nil, fmt.Errorf("no service providers")
	}

	// http client
	geoClient := &http.Client{
		Timeout:   TimeOutGeolocation,
		Transport: &insecureTransport,
	}

	// call each service with licensed token
	var (
		resultMu sync.Mutex
		result   = make(GeolocationResult)
		wg       = new(errgroup.Group)
	)
	for name, token := range allToken {
		func(name GeolocationServiceProvider, cfg *geolocSvcProviderConfig, token string) {
			wg.Go(func() error {
				loc, err := queryGeolocService(geoClient, ip, cfg, token)
				if loc != nil {
					resultMu.Lock()
					result[name] = loc
					resultMu.Unlock()
					return nil
				}
				return err
			})
		}(name, geolocSvcProviders[name], token)
	}

	// wait for all results and the first error
	err := wg.Wait()

	// ignore the error if got any result
	if len(result) > 0 {
		return result, nil
	}
	return nil, err
}

func queryGeolocService(httpClient *http.Client, ip net.IP, svc *geolocSvcProviderConfig, token string) (*Geolocation, error) {
	url := fmt.Sprintf(strings.ReplaceAll(svc.urlTemplate, licensePlaceholder, token), ip.String())
	resp, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if !(200 <= resp.StatusCode && resp.StatusCode < 300) {
		return nil, fmt.Errorf("invalid status: %s", resp.Status)
	}

	doc, err := jsonquery.Parse(resp.Body)
	if err != nil {
		return nil, err
	}
	loc := Geolocation{}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.CountryCode); n != nil {
		loc.CountryCode = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.CountryFlag); n != nil {
		loc.CountryFlag = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.Country); n != nil {
		loc.Country = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.Region); n != nil {
		loc.Region = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.City); n != nil {
		loc.City = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.Latitude); n != nil {
		loc.Latitude = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.Longitude); n != nil {
		loc.Longitude = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.Carrier); n != nil {
		loc.Carrier = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.ISP); n != nil {
		loc.ISP = n.InnerText()
	}
	if n, _ := jsonquery.Query(doc, svc.fieldPath.ASN); n != nil {
		loc.ASN = n.InnerText()
	}
	return &loc, nil
}

func (g GeolocationResult) String() string {
	strJoin := func(elems ...string) string {
		var newElems []string
		for _, e := range elems {
			if len(e) > 0 {
				newElems = append(newElems, e)
			}
		}
		return strings.Join(newElems, ", ")
	}
	rm := make(map[string]struct{})
	for _, v := range g {
		var s string
		if len(v.Country) > 0 {
			s = strJoin(v.City, v.Region, v.Country)
		} else {
			s = strJoin(v.City, v.Region, v.CountryCode)
		}
		rm[s] = struct{}{}
	}
	var rl []string
	for k := range rm {
		if len(k) > 0 {
			rl = append(rl, k)
		}
	}
	sort.Strings(rl)
	return "{" + strings.Join(rl, " | ") + "}"
}

// GetCity returns the most frequent city name in the result.
func (g GeolocationResult) GetCity() string {
	// convert into map of counting
	var prefer string
	cities := make(map[string]int, len(g))
	for p, m := range g {
		var cc string
		switch {
		case isNotBlank(m.City):
			cc = m.City
		case isNotBlank(m.Region):
			cc = m.Region
		case isNotBlank(m.Country):
			cc = m.Country
		case isNotBlank(m.CountryCode):
			cc = m.CountryCode
		default:
			continue
		}
		// count the city or region or country
		cities[cc]++
		if p == ServiceProviderIpdata {
			prefer = cc
		}
	}
	return mergeStatResult(cities, prefer)
}

// GetCountryCode returns the most frequent country code in the result.
func (g GeolocationResult) GetCountryCode() string {
	// convert into map of counting
	var prefer string
	countries := make(map[string]int, len(g))
	for p, m := range g {
		cc := m.CountryCode
		if isBlank(cc) {
			continue
		}
		// count the city or region or country
		countries[cc]++
		if p == ServiceProviderIpdata {
			prefer = cc
		}
	}
	return mergeStatResult(countries, prefer)
}

// mergeStatResult merges the counting result of different service providers.
// If there are any keys that have a counter that is more than 1, return the most frequent one.
// If all results are different, i.e. counters of all keys are 1, return non-blank preferred result.
// Otherwise, return the random one.
func mergeStatResult(counters map[string]int, preferred string) string {
	// just in case got no result
	if len(counters) == 0 {
		return ""
	}
	// find the most frequent key
	type kv struct {
		Key   string
		Value int
	}
	var ss []kv
	for k, v := range counters {
		ss = append(ss, kv{k, v})
	}
	// sort by value
	sort.SliceStable(ss, func(i, j int) bool {
		return ss[i].Value > ss[j].Value
	})
	if ss[0].Value > 1 {
		// if got more than one value, return the most frequent one
		return ss[0].Key
	} else if isNotBlank(preferred) {
		// if all results are different, return non-blank preferred result
		return preferred
	} else {
		// or return the 'first' one as fallback (actually random one)
		return ss[0].Key
	}
}

func isBlank(s string) bool {
	return strings.TrimSpace(s) == ""
}

func isNotBlank(s string) bool {
	return strings.TrimSpace(s) != ""
}
