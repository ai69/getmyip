package getmyip

import (
	"testing"
)

func TestGetOutboundIP(t *testing.T) {
	got, err := GetOutboundIP()
	if err != nil {
		t.Errorf("got error: %v", err)
	}
	t.Logf("outbound ip: %v", got.String())
}

func BenchmarkGetOutboundIP(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = GetOutboundIP()
	}
}
