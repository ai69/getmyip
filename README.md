# getmyip [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/bitbucket.org/ai69/getmyip)

Package getmyip provides methods to get public & outbound IP address of the current node.

## install

```bash
go get -d bitbucket.org/ai69/getmyip
```
