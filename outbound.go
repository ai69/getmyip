package getmyip

import (
	"context"
	"net"
)

var (
	remoteAddrs = []string{
		"1.1.1.1:80",
		"8.8.8.8:80",
		"9.9.9.9:80",
	}
)

// GetOutboundIP retrieves preferred outbound IP address of current machine.
func GetOutboundIP() (net.IP, error) {
	return parallelProcess(remoteAddrs, queryOutboundIP, TimeOutOutboundIP)
}

func queryOutboundIP(ctx context.Context, addr string) (net.IP, error) {
	var d net.Dialer
	conn, err := d.DialContext(ctx, "udp", addr)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	local := conn.LocalAddr().(*net.UDPAddr)
	return local.IP, nil
}
