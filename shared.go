package getmyip

import (
	"context"
	"crypto/tls"
	"net"
	"net/http"
	"time"

	"golang.org/x/sync/errgroup"
)

var (
	insecureTransport = http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
)

// parallelProcess runs the given workloads with given parameters in parallel and returns the first succeeded result, or returns error if got no results.
// workFunc should handle the cancellation of context in time or timeout won't work.
func parallelProcess(params []string, workFunc func(context.Context, string) (net.IP, error), timeout time.Duration) (net.IP, error) {
	var resultC = make(chan net.IP, len(params))
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	g := new(errgroup.Group)
	for _, p := range params {
		func(a string) {
			g.Go(func() error {
				ip, err := workFunc(ctx, a)
				if err != nil {
					return err
				}
				resultC <- ip
				cancel()
				return nil
			})
		}(p)
	}

	err := g.Wait()
	cancel()
	select {
	case ip := <-resultC:
		return ip, nil
	default:
		return nil, err
	}
}

// IsPublicIP checks if the given IP is a public IP, i.e. not loopback, link-local, interface-local, unspecified, or multicast.
func IsPublicIP(ip net.IP) bool {
	if ip == nil {
		return false
	}
	isNonPublic := ip.IsLoopback() || ip.IsLinkLocalUnicast() || ip.IsLinkLocalMulticast() || ip.IsInterfaceLocalMulticast() || ip.IsUnspecified() || ip.IsMulticast()
	return !isNonPublic
}
