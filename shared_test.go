package getmyip

import (
	"net"
	"testing"
)

func TestIsPublicIP(t *testing.T) {
	tests := []struct {
		ipAddress string
		want      bool
	}{
		{"", false},
		{"127.0.0.1", false},
		{"::1", false},
		{"169.254.0.1", false},
		{"fe80::1", false},
		{"ff02::1", false},
		{"0.0.0.0", false},
		{"::", false},
		{"224.0.0.1", false},
		{"8.8.8.8", true},
		{"2001:4860:4860::8888", true},
		{"198.51.100.1", true},
		{"203.0.113.1", true},
		{"2001:0db8:85a3:0000:0000:8a2e:0370:7334", true},
		{"2002:c0a8:6301::", true},
	}
	for _, tt := range tests {
		t.Run(tt.ipAddress, func(t *testing.T) {
			ip := net.ParseIP(tt.ipAddress)
			if got := IsPublicIP(ip); got != tt.want {
				t.Errorf("IsPublicIP(%s) = %v, want %v", tt.ipAddress, got, tt.want)
			}
		})
	}
}
