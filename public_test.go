package getmyip

import (
	"testing"
)

func TestGetPublicIP(t *testing.T) {
	got, err := GetPublicIP()
	if err != nil {
		t.Errorf("got error: %v", err)
	}
	t.Logf("public ip: %v", got.String())
}

func TestGetPublicIPWithClient(t *testing.T) {
	got, err := GetPublicIPWithClient(nil, TimeOutPublicIP)
	if err != nil {
		t.Errorf("got error: %v", err)
	}
	t.Logf("public ip: %v", got.String())
}

func BenchmarkGetPublicIP(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = GetPublicIP()
	}
}
