package getmyip

import "time"

// DefaultGeolocationToken contains access tokens for various Geolocation API, you may bring your own license by overriding this variable, leave it value empty if it requires no license.
var DefaultGeolocationToken = GeolocationAccessTokens{
	ServiceProviderIpapiCo:  "",
	ServiceProviderIpApiCom: "",
}

var (
	// TimeOutGeolocation is the timeout for querying Geolocation API.
	TimeOutGeolocation = 15 * time.Second

	// TimeOutPublicIP is the timeout for querying Public IP API.
	TimeOutPublicIP = 10 * time.Second

	// TimeOutOutboundIP is the timeout for querying Outbound IP.
	TimeOutOutboundIP = 10 * time.Millisecond
)
