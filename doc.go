/*
Package getmyip returns public and outbound IP addresses of current machine.
*/
package getmyip
