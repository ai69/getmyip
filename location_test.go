package getmyip

import (
	"fmt"
	"net"
	"testing"
)

func TestGetGeolocation(t *testing.T) {
	// check nil case
	if _, err := GetGeolocation(nil); err == nil {
		t.Errorf("should return error for nil ip")
	}

	// get current IP for testing
	ip, err := GetPublicIP()
	if err != nil {
		t.Errorf("fail to get public ip: %v", err)
		return
	}

	DefaultGeolocationToken = nil
	if _, err = GetGeolocation(ip); err == nil {
		t.Errorf("should get error for no providers")
		return
	}
	t.Logf("got error: %v", err)

	DefaultGeolocationToken = GeolocationAccessTokens{
		ServiceProviderIpapiCo: "",
	}
	_ = checkGetGeolocationIP(t, "ipapi.co", ip)

	DefaultGeolocationToken = GeolocationAccessTokens{
		ServiceProviderIpApiCom: "",
	}
	_ = checkGetGeolocationIP(t, "ip-api.com", ip)

	if r, err := GetGeolocation(ip, GeolocationAccessTokens{
		ServiceProviderIpregistry: "wrong_token",
	}, nil); err != nil {
		t.Errorf("should get no error")
		return
	} else if cnt := len(r); cnt != 1 {
		t.Errorf("should get exactly 1 result: got %v", cnt)
	} else {
		t.Logf("got 1 result: %v, error: %v", r, err)
	}
}

func TestGetGeolocationResult(t *testing.T) {
	tests := []struct {
		provider GeolocationServiceProvider
		ip       string
		city     string
		region   string
	}{
		{ServiceProviderIpapiCo, "137.184.120.224", "Santa Clara", "California"},
		{ServiceProviderIpApiCom, "137.184.120.224", "Santa Clara", "California"},
		{ServiceProviderIpapiCo, "175.45.176.91", "Pyongyang", "Pyongyang"},
		{ServiceProviderIpApiCom, "175.45.176.91", "Pyongyang", "Pyongyang"},
		{ServiceProviderIpapiCo, "208.111.56.23", "Honolulu", "Hawaii"},
		{ServiceProviderIpApiCom, "208.111.56.23", "Honolulu", "Hawaii"},
	}
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s@%v", tt.ip, tt.provider), func(t *testing.T) {
			// set default provider
			switch tt.provider {
			case ServiceProviderIpapiCo:
				DefaultGeolocationToken = GeolocationAccessTokens{
					ServiceProviderIpapiCo: "",
				}
			case ServiceProviderIpApiCom:
				DefaultGeolocationToken = GeolocationAccessTokens{
					ServiceProviderIpApiCom: "",
				}
			default:
				t.Errorf("unknown provider: %v", tt.provider)
			}
			// query for result geolocation
			r := checkGetGeolocationIP(t, string(tt.provider), net.ParseIP(tt.ip))
			if len(r) == 0 {
				t.Errorf("got no result for [%v]", tt.ip)
			}
			// check result
			var city, region string
			for _, i := range r {
				city, region = i.City, i.Region
				break
			}
			if city != tt.city || region != tt.region {
				t.Errorf("got wrong result for [%v], expect: (%s, %s), acutal: (%s, %s), got: %v", tt.ip, tt.city, tt.region, city, region, r)
			}
		})
	}
}

func checkGetGeolocationIP(t *testing.T, name string, ip net.IP) GeolocationResult {
	if r, err := GetGeolocation(ip); err != nil {
		t.Errorf("got unexpected error from %s for [%v]: %v", name, ip, err)
		return nil
	} else if cnt := len(r); cnt != 1 {
		t.Errorf("should get exactly 1 result from %s for [%v], got %v", name, ip, cnt)
	} else {
		t.Logf("got 1 result from %s for [%v]: %v", name, ip, r.String())
		return r
	}
	return nil
}

func BenchmarkGetGeolocation(b *testing.B) {
	ip, _ := GetPublicIP()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = GetGeolocation(ip)
	}
}

func TestMergeStatResult(t *testing.T) {
	testCases := []struct {
		counters  map[string]int
		preferred string
		expected  []string
	}{
		{
			map[string]int{"New York": 3, "San Francisco": 1},
			"",
			[]string{"New York"},
		},
		{
			map[string]int{"New York": 1, "San Francisco": 1},
			"Honolulu",
			[]string{"Honolulu"},
		},
		{
			map[string]int{"San Francisco": 2},
			"New York",
			[]string{"San Francisco"},
		},
		{
			map[string]int{"San Francisco": 1},
			"New York",
			[]string{"New York"},
		},
		{
			map[string]int{"San Francisco": 1},
			"",
			[]string{"San Francisco"},
		},
		{
			map[string]int{},
			"New York",
			[]string{""},
		},
		{
			map[string]int{"New York": 1, "San Francisco": 2, "Los Angeles": 2},
			"Honolulu",
			[]string{"San Francisco", "Los Angeles"},
		},
		{
			map[string]int{"New York": 1, "San Francisco": 1},
			"",
			[]string{"New York", "San Francisco"},
		},
	}

	for _, testCase := range testCases {
		result := mergeStatResult(testCase.counters, testCase.preferred)
		found := false
		for _, expected := range testCase.expected {
			if result == expected {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("mergeStatResult(%v, %q) = %q; expected one of %v", testCase.counters, testCase.preferred, result, testCase.expected)
		}
	}
}

func TestGetCityAndCountryCode(t *testing.T) {
	testCases := []struct {
		geolocationResult GeolocationResult
		expectedCities    []string
		expectedCountries []string
	}{
		{
			GeolocationResult{
				ServiceProviderIpregistry: {"US", "", "United States", "New York", "New York", "", "", "", "", ""},
				ServiceProviderIpify:      {"US", "", "United States", "California", "San Francisco", "", "", "", "", ""},
				ServiceProviderIpApiCom:   {"US", "", "United States", "New York", "New York", "", "", "", "", ""},
			},
			[]string{"New York"},
			[]string{"US"},
		},
		{
			GeolocationResult{
				ServiceProviderIpregistry: {"US", "", "United States", "", "", "", "", "", "", ""},
				ServiceProviderIpify:      {"US", "", "United States", "California", "San Francisco", "", "", "", "", ""},
				ServiceProviderIpApiCom:   {"US", "", "United States", "New York", "New York", "", "", "", "", ""},
			},
			[]string{"New York", "San Francisco", "United States"},
			[]string{"US"},
		},
		{
			GeolocationResult{
				ServiceProviderIpregistry: {"US", "", "United States", "", "", "", "", "", "", ""},
				ServiceProviderIpify:      {"US", "", "United States", "", "", "", "", "", "", ""},
				ServiceProviderIpApiCom:   {"US", "", "United States", "", "", "", "", "", "", ""},
			},
			[]string{"United States"},
			[]string{"US"},
		},
		{
			GeolocationResult{
				ServiceProviderIpregistry: {"US", "", "United States", "New York", "New York", "", "", "", "", ""},
				ServiceProviderIpify:      {"US", "", "United States", "California", "San Francisco", "", "", "", "", ""},
				ServiceProviderIpdata:     {"US", "", "United States", "California", "Los Angeles", "", "", "", "", ""},
			},
			[]string{"Los Angeles"},
			[]string{"US"},
		},
	}

	for _, testCase := range testCases {
		cityResult := testCase.geolocationResult.GetCity()
		countryResult := testCase.geolocationResult.GetCountryCode()

		foundCity := false
		for _, expectedCity := range testCase.expectedCities {
			if cityResult == expectedCity {
				foundCity = true
				break
			}
		}

		if !foundCity {
			t.Errorf("GetCity() = %q; expected one of %v", cityResult, testCase.expectedCities)
		}

		foundCountry := false
		for _, expectedCountry := range testCase.expectedCountries {
			if countryResult == expectedCountry {
				foundCountry = true
				break
			}
		}

		if !foundCountry {
			t.Errorf("GetCountryCode() = %q; expected one of %v", countryResult, testCase.expectedCountries)
		}
	}
}
